import React from 'react';
import BASE_URL from './Constant';
import axios from 'axios'
import { withRouter } from 'react-router-dom';
import './ListBoard.css'

export class ListBoard extends React.Component {

	state = {
		boards: [],
		userId: ''
	}

	componentDidMount() {
		axios.get(`${BASE_URL}/boards`)
			.then((response) => {
				const boardsData = response.data;
				this.setState({ boards: boardsData });
			});
		this.setState({ userId: window.localStorage.getItem('userId') })
	}

	postParticipant(boardId, userId) {
		axios.post(`${BASE_URL}/boards/${boardId}/user/${userId}`)
	}

	render() {
		return (
			<div id="content-div">
				<h1 id="title" align="center"> List Boards </h1>
				<div id="form-table">
					<table id="table">
						<thead>
							<tr>
								<th>Board Id</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							{this.state.boards.map(board =>
								<tr>
									<td>{board.id}</td>
									<td>{board.title}</td>
									<td>{board.description}</td>
									<button onClick={() => {
										this.postParticipant(board.id, window.localStorage.getItem('userId'));
										this.props.history.push(`/board/${board.id}`);
										window.location.reload();
									}}>
										Join Board
													</button>
								</tr>
							)}
						</tbody>
					</table>
				</div>
			</div>
		)
	}
}

export default withRouter(ListBoard);