import React from 'react';
import './App.css';
import Board from './Board';
import BoardForm from './BoardForm';
import Login from './Login';
import ListBoard from './ListBoard';

import {
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div id="content-div">
      <nav>
        <ul>
          <li>
            <a href="/login">Log In</a>
          </li>
          <li>
            <a href="/createboard">Create Board</a>
          </li>
          <li>
            <a href="/listboards">List Boards</a>
          </li>
        </ul>
      </nav>

      <Switch>
        <Route exact path="/board/:id" component={Board}></Route>
        <Route exact path="/createboard" component={BoardForm}></Route>
        <Route exact path="/listboards" component={ListBoard}></Route>
        <Route exact path="/login"> <Login /> </Route>
        <Route exact path="/"> <Login /> </Route>
      </Switch>
    </div>
  );
}

export default App;
