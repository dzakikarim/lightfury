import React from 'react';
import { shallow } from 'enzyme';
import User from './User';

let wrapper;

describe('User', () => {
    it('should render card with argument & likes', () => {
        wrapper = shallow(<User id="1" name="wishnu" />);

        expect(wrapper.find('#userId').text()).toEqual('1');
        expect(wrapper.find('#userName').text()).toEqual('wishnu');
    })
})