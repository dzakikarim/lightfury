import React from 'react';
import './Section.css';
import Card from './Card';
import BASE_URL from './Constant';
import axios from 'axios'
import { IconButton } from '@material-ui/core';
import { FaSortAmountDown, FaSortAmountUp } from 'react-icons/fa';
import { MdAddCircle } from "react-icons/md";

class Section extends React.Component {
    constructor() {
        super();
        this.saveArgument = this.saveArgument.bind(this);
    }
    state = {
        cards: [],
        newCard: {
            argument: "default"
        },
        sortByAscending: false
    }

    async componentDidMount() {
        await axios.get(`${BASE_URL}/sections/${this.props.id}/cards`)
            .then((response) => {
                const cardData = response.data;
                this.setState({ cards: cardData });
            });
    }

    handleDelete(deletedCard) {
        if (deletedCard.user.id === window.localStorage.getItem('userId')) {
            axios.delete(`${BASE_URL}/cards/${deletedCard.id}/users/${window.localStorage.getItem('userId')}`)
            const cardsData = [...this.state.cards];
            const deletedCards = cardsData.filter(card => (card.id !== deletedCard.id));
            this.setState({
                cards: deletedCards
            })
        }
    }

    saveArgument = (event) => {
        const newArgument = event.target.value;

        this.setState({
            newCard: {
                argument: newArgument
            }
        })
    }

    postCard = (event) => {
        let newArgument = this.state.newCard.argument.trim();
        if (this.state.newCard.argument !== "default" && newArgument.length !== 0) {
            axios.post(`${BASE_URL}/sections/${this.props.id}/cards/${window.localStorage.getItem('userId')}`, this.state.newCard);
        }
    }

    addLike(cardId) {
        axios.post(`${BASE_URL}/cards/${cardId}/likes/${window.localStorage.getItem('userId')}`)
        window.location.reload()
    }

    _toggleSort() {
        const cardsData = [...this.state.cards];
        const sortCard = cardsData.sort((a, b) => {
            if (this.state.sortByAscending) {
                return b.likes.length - a.likes.length;
            }
            return a.likes.length - b.likes.length;
        });
        this.setState({
            sortByAscending: !this.state.sortByAscending,
            cards: sortCard
        });
    }

    render() {
        return (
            <div className="sectionName">
                <h1 className="title">{this.props.title}</h1>
                <IconButton className="sortButton" onClick={() => this._toggleSort()}>
                    {this.state.sortByAscending ? <FaSortAmountUp /> : <FaSortAmountDown />}
                </IconButton>
                <form className="args" onSubmit={this.postCard}>
                    <textarea type="text" id="argument" placeholder="Put your argument here" rows="5" cols="50" onChange={this.saveArgument}></textarea>
                    <button className="addBtn" type="submit"><MdAddCircle size={30} /></button>
                </form>
                {this.state.cards.map( card => {
                    if(card.user.id !== window.localStorage.getItem('userId')){
                        return <Card
                        argument={card.argument}
                        addLike={() => this.addLike(card.id)}
                        likes={card.likes}
                        delete={false}/>
                    }
                    return <Card
                    argument={card.argument}
                    addLike={() => this.addLike(card.id)}
                    likes={card.likes}
                    delete={true}
                    handleDelete={() => this.handleDelete(card)}/>
                }
                )}
            </div>
        )
    }
}

export default Section;