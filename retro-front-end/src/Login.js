import React from 'react';
import './Login.css'
import axios from 'axios'
import { withRouter } from 'react-router-dom';
import BASE_URL from './Constant';

export class Login extends React.Component {

	state = {
		user: {
			username: '',
			password: ''
		},
		error: ''
	}

	_handleChange = (event) => {
		this.setState({
			user: {
				...this.state.user,
				[event.target.name]: event.target.value
			}
		})
	}

	_login = (event) => {
		event.preventDefault();
		axios.get(`${BASE_URL}/login?password=${this.state.user.password}&username=${this.state.user.username}`)
			.then(response => {
				if (response.status === 200) {
					window.localStorage.setItem('userId', response.data.id);
					window.localStorage.setItem('userName', response.data.username);
					this.props.history.push(`/listboards`);
				}
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ error: 'Invalid username or password' })
			})
	}

	render() {
		return (
			<div>
				<h1 id="login-title">{"Log In"}</h1>
				<form className="login-form" id="=login-form" onSubmit={this._login}>
					<div className="login-body">

						<h3 className="content">Username : </h3>
						<input type="text" id="username" placeholder="input username" name="username" onChange={this._handleChange}></input>

						<h3 className="content">Password : </h3>
						<input type="password" id="password" placeholder="input password" name="password" onChange={this._handleChange}></input>

						<div className="error">
							<h3 id="error-message">{this.state.error}</h3>
						</div>

						<button className="contact2-form-btn" type="submit">Log In</button>
					</div>
				</form>
			</div>
		)
	}
}

export default withRouter(Login);