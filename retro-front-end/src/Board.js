import React from 'react';
import Section from './Section';
import User from './User';
import './Board.css';
import axios from 'axios';
import BASE_URL from './Constant';

class Board extends React.Component {
    state = {
        sections: [],
        board: [],
        participant: []
    }

    async componentDidMount() {
        try {
            const { data: board } = await axios.get(`${BASE_URL}/boards/${this.props.match.params.id}`);
            const { data: sections } = await axios.get(`${BASE_URL}/boards/${this.props.match.params.id}/sections`);
            this.setState({ board, sections });
            this.setState({ participant : this.state.board.participant});
        } catch (error) {
            this.setState({ errorMessage: error.message });
        }
    }

    render() {
        return (
            <div className="main-board">
                <h1>{this.state.board.title}</h1>
                <h4>Board's ID : {this.state.board.id}</h4>
                <div className="participant">
                    <h3>Participant: </h3>
                    {
                        this.state.participant.map(user => 
                            <User name={user.username} />
                    )}
                </div>
                <div className="section-container">
                    {this.state.sections.map(section =>
                        <Section title={section.title} id={section.id} />
                    )}
                </div>
            </div>

        )
    }
}

export default Board;