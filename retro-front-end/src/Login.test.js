import React from 'react';
import { Login } from "./Login";
import { shallow } from 'enzyme';
import axios from 'axios';

jest.mock('axios');

describe('Login', () => {

    it('should invoked post when submit board form', (done) => {
        const data = {
						id: "1",
            username: "budi",
            password: "1234"
        };
        axios.get.mockResolvedValue({ data });
        const wrapper = shallow(<Login />)

        const usernameEvent = { target: { name: "username", value: "budi" } };
        const passwordEvent = { target: { name: "password", value: "1234" } };
        wrapper.find("#username").props().onChange(usernameEvent);
        wrapper.find("#password").props().onChange(passwordEvent);
        wrapper.find(".login-form").simulate('submit', { preventDefault: jest.fn() })

        setImmediate(() => {
            expect(axios.get).toHaveBeenCalled();
            done();
        })
    });
});
