import React from 'react';
import './BoardForm.css';
import axios from 'axios'
import BASE_URL from './Constant';
import { withRouter } from 'react-router-dom';

export class BoardForm extends React.Component {
    state = {
        boards: [],
        newBoard: {
            title: '',
            description: ''
        },
        newBoardId: '',
        redirect: false
    }

    saveBoard = (event) => {
        this.setState({
            newBoard: {
                ...this.state.newBoard,
                [event.target.name]: event.target.value
            }
        });
    }

    postBoard = (event) => {
        event.preventDefault();
        if (this.state.newBoard.title !== '' && this.state.newBoard.title.trim().length !== 0) {
            axios.post(`${BASE_URL}/users/${window.localStorage.getItem('userId')}/boards`, this.state.newBoard)
                .then(response => {
                    this.props.history.push(`/board/${response.data.id}`);
                });
        }
    }

    render() {
        return (
            <div className="main-page-board">
                <h1 id="board-title">{"Create Board"}</h1>
                <form className="board-form" id="=Board-form" onSubmit={this.postBoard}>
                    <div className="board-body">
                        <h3 className="content">Title : </h3>
                        <input type="text" id="Title" placeholder="input title" name="title" value={this.state.newBoard.title} onChange={this.saveBoard}></input>
                        
                        <h3 className="content">Description : </h3>
                        <textarea name="description" placeholder="input description" id="Description" value={this.state.newBoard.description} onChange={this.saveBoard}></textarea>

                        <button className="contact2-form-btn" type="submit">create</button>
                    </div>
                </form>
            </div>

        )
    }
}

export default withRouter(BoardForm);
