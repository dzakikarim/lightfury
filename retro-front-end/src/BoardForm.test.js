import React from 'react';
import { BoardForm } from "./BoardForm";
import { shallow } from 'enzyme';
import axios from 'axios';

jest.mock('axios');

describe('BoardForm', () => {

    it('should invoked post when submit board form', (done) => {
        const data = {
            title: "title 1",
            description: "isi dari title 1"
        };
        axios.post.mockResolvedValue({ data });
        const wrapper = shallow(<BoardForm userId="1" history={[]} />)

        const titleEvent = { target: { name: "title", value: "title 1" } };
        const descriptionEvent = { target: { name: "description", value: "isi dari title 1" } };
        wrapper.find("#Title").props().onChange(titleEvent);
        wrapper.find("#Description").props().onChange(descriptionEvent);
        wrapper.find(".board-form").simulate('submit', { preventDefault: jest.fn() })

        setImmediate(() => {
            expect(axios.post).toHaveBeenCalled();
            done();
        })
    });
});
