import React from 'react';
import { shallow } from 'enzyme';
import Section from './Section';
import axios from 'axios';
import BASE_URL from './Constant';

jest.mock('axios');

describe('Section', () => {
    let wrapper;
    let response;
    const cards = [
        { argument: "Good teamwork", likes: 18, user: {id: '1'} },
        { argument: "Good discussion", likes: 21, user: {id: '2'} }
    ];

    beforeEach(() => {
        response = { data: cards };
        axios.get.mockResolvedValue(response);
        wrapper = shallow(<Section title={'Need to improve'} />);
    });

    it('should render title', (done) => {
        setImmediate(() => {
            expect(wrapper.find('.title').text()).toEqual('Need to improve');
            done();
        });
    });

    it('should render Cards with argument & total likes', (done) => {
        setImmediate(() => {
            expect(wrapper.find('Card').exists()).toBe(true);
            expect(wrapper.find('Card')).toHaveLength(2);
            done();
        });
    });

    it('should render sorted cards when sort button is pressed ascending', () => {
        wrapper.find('.sortButton').simulate('click');
        expect(wrapper.find('Card').at(0).props().likes).toBeLessThanOrEqual(wrapper.find('Card').at(1).props().likes); 
    });

      it('should called post method when form is submitted', (done) => {
        response = { data: cards};
        axios.post.mockResolvedValue(response);
        wrapper = shallow(<Section id="sectionId" />);

        const argumentEvent = { target: {value: "Good teamwork"}};
        wrapper.find('#argument').props().onChange(argumentEvent);
        wrapper.find('.args').simulate('submit', { preventDefault: () =>{} })
        
        setImmediate( () => {
            expect(axios.post).toHaveBeenCalled();
            done();
        });
      });
});