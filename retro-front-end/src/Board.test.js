import React from 'react';
import { shallow } from 'enzyme';
import Board from './Board';
import axios from 'axios';

jest.mock('axios');

describe('Board', () => {
    let wrapper;
    const board = { id: "101", title: 'Awesome Squad Retro Sprint 30' };
    const sections = [{ id: '0',name: 'What went well'}];

    beforeEach(() => {
        let response = { data: board };
        axios.get.mockResolvedValueOnce(response);
        response = { data: sections };
        axios.get.mockResolvedValue(response);
        wrapper = shallow(
            <Board
              match={{params: {id:101} }}
            />
          );
    });

    it('should render title and id', (done) => {
        setImmediate(() => {
            expect(wrapper.find('h1').text()).toEqual(board.title);
            expect(wrapper.find('h4').text()).toEqual("Board's ID : " + board.id);
            done();
        });
    });

    it('should render sections', (done) => {
        setImmediate(() => {
            expect(wrapper.find('Section').at(0).props().id).toEqual(sections[0].id);
            done();
        });
    });
});