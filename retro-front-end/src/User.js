import React from 'react';
//import './User.css';


function User(props) {
    return (
        <div className="user">
            <h5 id="userId">{props.id}</h5>
            <h5 id="userName">{props.name}</h5>
        </div>
    );
}

export default User;