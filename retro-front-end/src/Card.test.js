import React from 'react';
import { shallow } from 'enzyme';
import Card from './Card';

let wrapper;

describe('Card', () => {
    it('should render card with argument & likes', () => {
        wrapper = shallow(<Card argument="Ini argument 1" likes={[]} />);

        expect(wrapper.find('#card-argument').text()).toEqual('Ini argument 1');
        expect(wrapper.find('#likebtn').text()).toEqual('👍0');
    })

    it('should delete card when handleDelete is invoked', () =>{
        const mockDelete = jest.fn();

        const wrapper = shallow(<Card handleDelete={mockDelete} delete={true} likes={[]}/>);
        wrapper.find('#deletebtn').simulate('click');

        expect(mockDelete).toHaveBeenCalled();
    })

    it('should add like card when addLike is invoked', () =>{
        const mockLike = jest.fn();

        const wrapper = shallow(<Card addLike={mockLike}  likes={[]}/>);
        wrapper.find('#likebtn').simulate('click');

        expect(mockLike).toHaveBeenCalled();
    })
})