import React from 'react';
import App from './App';
import { mount } from 'enzyme';
import BoardForm from './BoardForm';
import { MemoryRouter } from 'react-router-dom';
import ListBoard from './ListBoard';
import axios from 'axios';
let wrapper;
jest.mock('axios');

describe('App', () => {

   const board = [
      {
         id: "1",
         title: "test",
         desciption: "isi test"
      }
   ]

   it('should render component board form', () => {
      wrapper = mount(<MemoryRouter initialEntries={['/createboard']}>
         <App />
      </MemoryRouter>);

      expect(wrapper.find(BoardForm).exists()).toBe(true);
      expect(wrapper.find(ListBoard).exists()).toBe(false);
   });

   it('should render component list board', () => {
      let resp = { data: board }
      axios.get.mockResolvedValue(resp);
      wrapper = mount(<MemoryRouter initialEntries={['/listboards']}>
         <App />
      </MemoryRouter>);
      
      expect(wrapper.find(BoardForm).exists()).toBe(false);
      expect(wrapper.find(ListBoard).exists()).toBe(true);
   });
});