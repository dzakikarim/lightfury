import React from 'react';
import './Card.css';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';


class Card extends React.Component {
    render() {
        return (
            <div className="card">
                <p id="card-argument">{this.props.argument}</p>

                {
                    this.props.delete ?
                        (
                            <div className="btn-container">
                                <button id="likebtn" onClick={this.props.addLike}>&#128077;{this.props.likes.length}</button>
                                <IconButton id="deletebtn" onClick={this.props.handleDelete}>
                                    <DeleteIcon />
                                </IconButton>
                            </div>
                        ) :
                        (
                            <div className="btn-container">
                                <button id="likebtn" onClick={this.props.addLike}>&#128077;{this.props.likes.length}</button>
                            </div>
                        )
                }
            </div>
        );
    }
}

export default Card;