import React from 'react';
import { ListBoard } from "./ListBoard";
import { shallow } from 'enzyme';
import axios from 'axios';
import BASE_URL from './Constant'
jest.mock('axios');

describe('ListBoard', () => {

    const board = [
        {
            id: "0",
            title: "board1",
            descrption: "description board 1"
        }
    ]

    const user = [
        {
            userdId: "1",
            board : {Id:"2"}
        }
    ]

    let response;
    let wrapper;

    beforeEach(() => {
        response = { data: board };
        axios.get.mockResolvedValue(response)
        response = {data:user}
        axios.post.mockResolvedValue(response)
        wrapper = shallow(<ListBoard userId="1" boardId="2" history={[]} />);
    })

    it('should render list of boards', (done) => {
        setImmediate(() => {
            expect(axios.get).toHaveBeenCalledWith(`${BASE_URL}/boards`)
            done();
        });
    });

    it('should post user to participant list', (done) => {
        wrapper.find('button').simulate('click');
        setImmediate(() => {
            expect(axios.post).toHaveBeenCalled();
            done();
        });
    });
});