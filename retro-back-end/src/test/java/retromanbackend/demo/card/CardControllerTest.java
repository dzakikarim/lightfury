package retromanbackend.demo.card;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import com.fasterxml.jackson.databind.ObjectMapper;

import retromanbackend.demo.section.Section;
import retromanbackend.demo.section.SectionRepository;

@AutoConfigureMockMvc
@SpringBootTest
public class CardControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private CardRepository cardRepository;

	@Autowired
	private SectionRepository sectionRepository;

	@BeforeEach
	void setup() {
		cardRepository.deleteAll();
		sectionRepository.deleteAll();
	}

	@Test
	public void create_shouldReturnStatusCode201_whenPostCard() throws Exception {
		Section wentWell = Section.builder().title("Went Well").build();
		String sectionId = sectionRepository.save(wentWell).getId();
		Card card = Card.builder().argument("Good Teamwork").build();
		cardRepository.save(card);

		RequestBuilder request = post("/sections/{sectionId}/cards", sectionId)
				.content(objectMapper.writeValueAsString(card))
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(request).andExpect(status().isCreated()).andReturn();
	}

	@Test
    public void findAllBySectionId_shouldReturnCardsWithSectionId_whenfindBySectionId() throws Exception {
        Section wentWell = Section.builder().title("Action Items").build();
		String sectionId = sectionRepository.save(wentWell).getId();
		Card card = Card.builder().argument("Good Teamwork").build();
		cardRepository.save(card);

		RequestBuilder request = get("/sections/{sectionId}/cards", sectionId)
				.content(objectMapper.writeValueAsString(card))
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(request).andExpect(status().isOk()).andReturn();
	}
	
	@Test
	public void deleteById_shouldReturnStatusCode200_whenCardWithIdZeroIsDeleted() throws Exception {
		Card card = Card.builder()
		.argument("Hahahaha")
		.build();
		cardRepository.save(card);
		RequestBuilder request = delete("/cards/{cardId}", card.getId());

		mockMvc.perform(request)
			.andExpect(status().isOk());

		assertFalse(cardRepository.existsById(card.getId()));
	}
}
