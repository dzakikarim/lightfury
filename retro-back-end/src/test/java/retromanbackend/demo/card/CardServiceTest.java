package retromanbackend.demo.card;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import retromanbackend.demo.section.Section;
import retromanbackend.demo.section.SectionRepository;
import retromanbackend.demo.section.SectionService;
import retromanbackend.demo.user.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CardServiceTest {
    private CardService cardService;
    
    @Mock
    private CardRepository cardRepository;

    @Mock
    private SectionService sectionService;

    @Mock
    private UserService userService;
    
    @BeforeEach
    void setup() {
        cardService = new CardService(cardRepository, sectionService, userService);
    }

    @Test
    public void findOneCardById_shouldReturnCardWithIdZero_whenFindOneCardWithIdZero() {
        Card card = Card.builder().id("0").argument("HAHAHA").build();
        when(cardRepository.findById(card.getId())).thenReturn(Optional.of(card));

        Card foundedCard = cardService.findById("0");
        
        assertEquals(card, foundedCard);
        verify(cardRepository).findById(card.getId());
    }

    @Test
    public void saveBySectionId_shouldReturnCardWithIdOne_whenFindOneCardWithIdOne() {
        Card card = Card.builder().id("1").argument("HAHAHAHA").build();

        Card createdCard = cardService.saveBySectionId(card);

        verify(cardRepository).save(card);
        assertEquals(card, createdCard);
    }

    @Test
    public void getCardBySectionId_shouldReturnCardWithSectionId1_whenFindOneCardWithSectionIdOne() {
        Section section = Section.builder().id("1").title("Hahaha").build();
        Card card = Card.builder().section(section).argument("HAHAHA").build();
        when(sectionService.findById(section.getId())).thenReturn(section);
        when(cardRepository.findAllBySection(section)).thenReturn(Arrays.asList(card));

        List<Card> listCard = cardService.getCardBySectionId(section.getId());

        verify(cardRepository).findAllBySection(section);
        assertEquals(listCard, Arrays.asList(card));
    }

    @Test
    public void deleteById_shouldDeleteCardByIdZero_whenCardFoundWithIdZero() {
        Card card = Card.builder().id("0").argument("HAHAHA").build();
        when(cardRepository.findById(card.getId())).thenReturn(Optional.of(card));
        cardRepository.save(card);
        cardService.deleteById(card.getId());

        assertFalse(cardRepository.existsById(card.getId()));
        verify(cardRepository).findById(card.getId());
    }
}