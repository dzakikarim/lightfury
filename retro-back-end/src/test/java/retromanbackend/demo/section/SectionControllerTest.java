package retromanbackend.demo.section;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import retromanbackend.demo.board.Board;
import retromanbackend.demo.board.BoardRepository;
import retromanbackend.demo.user.User;
import retromanbackend.demo.user.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

@AutoConfigureMockMvc
@SpringBootTest
public class SectionControllerTest {
    @Autowired
    private MockMvc app;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setup() {
        userRepository.deleteAll();
        boardRepository.deleteAll();
    }

    @Test
    public void getSectionByBoardId_shouldReturnSection_whenBoardIdIsOne() throws Exception {
        User user = User.builder()
            .username("name")
            .build();
        userRepository.save(user);
        Board board = Board.builder()
            .title("title")
            .description("description")
            .user(user)
            .build();
        boardRepository.save(board);
        sectionService.createDefaultSection(board);
        List<Section> expectedList = sectionRepository.findAllByBoard(board);
     
        RequestBuilder request = get("/boards/{boardId}/sections", board.getId());

        app.perform(request)
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(expectedList)));
    }
}