package retromanbackend.demo.section;

import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

import retromanbackend.demo.board.Board;
import retromanbackend.demo.board.BoardRepository;
import retromanbackend.demo.board.BoardService;

@ExtendWith(MockitoExtension.class)
public class SectionServiceTest {

    private SectionService sectionService;

    @Mock
    private BoardRepository boardRepository;
    private SectionRepository sectionRepository;
    
    @Mock
    private BoardService boardService;
    
    @BeforeEach
    void setup() {
        sectionService = new SectionService(sectionRepository, boardRepository);
    }
    
    @Test
    public void findById_returnSection_whenInvokedWithId() {
        Section section = Section.builder().id("1").title("Went Well").build();
        when(sectionRepository.findById(section.getId())).thenReturn(Optional.of(section));

        Section foundedSection = sectionService.findById("1");
        
        assertEquals(section, foundedSection);
        verify(sectionRepository).findById(section.getId());
    }

    @Test
    public void createDefaultSection_shouldSaveDefaultSection_whenInvoked() {
        Board board = Board.builder().id("1").title("Hahaha Board").build();

        sectionService.createDefaultSection(board);
    }

    @Test
    public void getSectionByBoardId_shouldReturnListOfSection_whenInvokedwithBoardId() {
        Board board = Board.builder().id("1").title("HAHAHA").build();
        Section section = Section.builder().board(board).title("Went Well").build();
        when(boardService.getBoardById(board.getId())).thenReturn(board);
        when(sectionRepository.findAllByBoard(board)).thenReturn(Arrays.asList(section));

        List<Section> listSection = sectionService.getSectionByBoardId(board.getId());
        
        verify(sectionRepository).findAllByBoard(board);
        assertEquals(listSection, Arrays.asList(section));
    }

}