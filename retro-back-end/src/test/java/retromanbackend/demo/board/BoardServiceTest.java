package retromanbackend.demo.board;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import retromanbackend.demo.section.SectionService;
import retromanbackend.demo.user.User;
import retromanbackend.demo.user.UserRepository;
import retromanbackend.demo.user.UserService;

@ExtendWith(MockitoExtension.class)
public class BoardServiceTest {
    @Mock
    private BoardRepository boardRepository;
    private BoardService boardService;

    @Mock
    private SectionService sectionService;

    @Mock
    private UserService userService;

    @BeforeEach
    public void setup() {
        boardService = new BoardService(boardRepository, userService, sectionService);
    }

    @Test
    public void getBoardById_shouldReturnBoardWithIdOne_whenBoardIdIsOne(){
        Board board = Board.builder().id("1").title("Hahahaha").build();
        when(boardRepository.findById(board.getId())).thenReturn(Optional.of(board));

        Board foundedBoard = boardService.getBoardById("1");
        
        assertEquals(board, foundedBoard);
        verify(boardRepository).findById(board.getId());
    }

    @Test
    public void getBoardByUserId_shouldReturnBoardWithUserIdOfOne_whenUserIdIsOne() {
        User user = User.builder().id("1").username("Hahaha").build();
        Board board = Board.builder().user(user).id("1").title("Hahahaha").build();
        when(userService.findById(user.getId())).thenReturn(user);
        when(boardRepository.findAllByUser(user)).thenReturn(Arrays.asList(board));

        List<Board> listBoard = boardService.getListByUserId(user.getId());

        verify(boardRepository).findAllByUser(user);
        assertEquals(listBoard, Arrays.asList(board));
    }

    @Test
    public void saveById_shouldReturnBoardsById_whenGetBoardById() {
        Board board = Board.builder().id("1").title("Hahahaha").build();
        Board createdCard = Board.builder().id("1").title("Hahahaha").build();
        
        verify(boardRepository).save(board);
        assertEquals(board, createdCard);
    }

    @Test
    public void fetchAll_shouldReturnAllBoards_whenFetchAllIsInvoked() {
        User user = User.builder().id("1").username("name").build();
        Board expectedBoard = Board.builder().title("title").description("description").user(user).build();
        when(boardRepository.findAll()).thenReturn(Arrays.asList(expectedBoard));

        List<Board> actualBoard = boardService.getList();

        assertEquals(Arrays.asList(expectedBoard), actualBoard);
        verify(boardRepository).findAll();
    }
}