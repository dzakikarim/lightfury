package retromanbackend.demo.board;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import retromanbackend.demo.section.*;
import retromanbackend.demo.user.User;
import retromanbackend.demo.user.UserRepository;

@AutoConfigureMockMvc
@SpringBootTest
public class BoardControllerTest {
    @Autowired
    private MockMvc app;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setup() {
        boardRepository.deleteAll();
    }

    @Test
    public void findAll_shouldReturnListOfBoard_whenBoardsAreFind() throws JsonProcessingException, Exception {
        Board board = Board.builder().title("title").description("description").build();
        RequestBuilder request = get("/boards");
        boardRepository.save(board);

        app.perform(request)
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(boardRepository.findAll())));
    }


    @Test
    public void createByUser_shouldReturnStatusCode201_whenBoardIsCreatedByUser() throws Exception {
        User user = User.builder().id("1").username("name").build();
        userRepository.save(user);
        Board board = Board.builder().id("1").user(user).title("title").description("description").build();

        RequestBuilder request = post("/users/{id}/boards", user.getId())
        .content(objectMapper.writeValueAsString(board)).contentType(MediaType.APPLICATION_JSON);

        app.perform(request).andExpect(status().isCreated());
        List<Section> actualListOfSection = sectionRepository.findAll();
        Section section1 = Section.builder().id(actualListOfSection.get(0).getId()).title("Went Well").board(board).build();
        Section section2 = Section.builder().id(actualListOfSection.get(1).getId()).title("Need to Improve").board(board).build();
        Section section3 = Section.builder().id(actualListOfSection.get(2).getId()).title("Action Items").board(board).build();
        List<Section> expectedListOfSection = new ArrayList<>(Arrays.asList(section1, section2, section3));
        
        assertEquals(expectedListOfSection, actualListOfSection);
    }

    @Test
    public void findAllByUserId_shouldReturnUserBoards_whenGetBoardsByUserId() throws Exception {
        User user = User.builder()
            .username("name")
            .build();
        User user2 = User.builder()
            .username("name2")
            .build();
        userRepository.save(user);
        userRepository.save(user2);
        Board board = Board.builder()
            .title("title")
            .description("description")
            .user(user)
            .build();
        Board board2 = Board.builder()
            .title("title2")
            .description("description2")
            .user(user2)
            .build();
        boardRepository.save(board);
        boardRepository.save(board2);
        List<Board> expectedList = new ArrayList<>();
        expectedList.add(board);

        RequestBuilder request = get("/users/{id}/boards", user.getId());
        app.perform(request).andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(expectedList)));
    }
}