package retromanbackend.demo.user;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.time.LocalDate;
import java.util.Optional;

import retromanbackend.demo.exception.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    private UserService userService;

    @BeforeEach
    void setup() {
        userService = new UserService(userRepository);
    }

    
    @Test
    public void findOneUserById_shouldReturnUserWithIdZero_whenFindOneUserWithIdZero() {
        User user = User.builder()
            .username("name")
            .id("0")
            .build();
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        User foundedUser = userService.findById("0");
        
        assertEquals(user, foundedUser);
        verify(userRepository).findById(user.getId());
    }

    @Test
    public void findById_shouldThrowNotFoundException_whenUserIdOneNotFound() {
        assertThrows(UserNotFoundException.class, () -> userService.findById("1"));
    }

    @Test
    public void updateById_shouldUpdateUserByIdZero_whenUserFoundWithIdZero() {
        User newUser = User.builder()
            .username("name")
            .id("0")
            .build();
        User updatedUser = userService.updateById("0", newUser);

        assertEquals(newUser, updatedUser);
        verify(userRepository).save(newUser);
    }

    @Test
    public void deleteById_shouldUpdateUserByIdZero_whenUserFoundWithIdZero() {
        User user = User.builder()
            .username("name")
            .id("0")
            .build();
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        userRepository.save(user);
        userService.deleteById(user.getId());

        assertFalse(userRepository.existsById(user.getId()));
        verify(userRepository).findById(user.getId());
    }
}