package retromanbackend.demo.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

	@Autowired
	private MockMvc app;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	void setup() {
		userRepository.deleteAll();
	}

	@Test
	public void create_shouldReturnStatus201_whenUserDataIsInsterted() throws Exception {
		User davidsen = User.builder()
			.username("Davidsen")
			.build();
		userRepository.save(davidsen);
		RequestBuilder request = post("/users")
			.content(objectMapper.writeValueAsString(davidsen))
			.contentType(MediaType.APPLICATION_JSON);

		app.perform(request)
			.andExpect(status().isCreated());

		assertTrue(userRepository.existsById(davidsen.getId()));
	}

	@Test
	public void findAll_shouldReturnList_whenFindAllIsInvoked() throws Exception {
		User davidsen = User.builder()
			.username("Davidsen")
			.build();
		userRepository.save(davidsen);
		RequestBuilder request = get("/users");

		app.perform(request)
			.andExpect(status().isOk())
			.andExpect(content().json(objectMapper.writeValueAsString(userRepository.findAll())));
	}

	@Test
	public void findOneUserById_shouldReturnOneUser_whenFineOneUserIsInvoked() throws Exception {
		User davidsen = User.builder()
			.username("Davidsen")
			.build();
		userRepository.save(davidsen);
		RequestBuilder request = get("/users/{id}", davidsen.getId());

		app.perform(request)
			.andExpect(status().isOk())
			.andExpect(content().json(objectMapper.writeValueAsString(davidsen)));
	}

	@Test
	public void findOneUserById_shouldThrowUserNotFoundExceptionAndShowStatus404_whenUserIdIsNotFound() throws Exception {
		String expectedId = "2";
		RequestBuilder request = get("/users/{id}", expectedId);

		app.perform(request)
			.andExpect(status().isNotFound());
	}

	@Test
	public void updateById_shouldReturnStatusCode200_whenIsIdZeroUpdated() throws Exception {
		User davidsen = User.builder()
			.username("Davidsen")
			.build();
		User davi = User.builder()
			.username("davi")
			.build();
		userRepository.save(davidsen);
		davi.setId(davidsen.getId());
		String updateUserString = objectMapper.writeValueAsString(davi);
		RequestBuilder request = put("/users/{id}", davidsen.getId())
			.content(updateUserString)
			.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = app.perform(request)
			.andExpect(status().isOk())
			.andReturn();
		User updatedUser = objectMapper.readValue(result.getResponse().getContentAsString(), User.class);

		assertEquals(davi,updatedUser);
	}

	@Test
	public void deleteById_shouldReturnStatusCode200_whenUserWithIdZeroIsDeleted() throws Exception {
		User davidsen = User.builder()
			.username("Davidsen")
			.build();
		userRepository.save(davidsen);
		RequestBuilder request = delete("/users/{id}", davidsen.getId());

		app.perform(request)
			.andExpect(status().isOk());

		assertFalse(userRepository.existsById(davidsen.getId()));
	}
}
