package retromanbackend.demo.board;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@CrossOrigin
@RestController
@EnableSwagger2
public class BoardController {

    @Autowired
    BoardService boardService;
    
    @PostMapping("/users/{userId}/boards")
    public ResponseEntity<Board> create(@PathVariable String userId, @RequestBody Board board) {
        Board createdBoard = boardService.saveById(userId, board);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdBoard);
    }
    
    @PostMapping("/boards/{boardId}/user/{userId}")
    public Board joinBoard(@PathVariable String boardId, @PathVariable String userId) {
        return boardService.joinBoard(boardId, userId);
    }

    @GetMapping("/users/{userId}/boards")
    public List<Board> findAllByUserId(@PathVariable String userId){
        return boardService.getListByUserId(userId);
    }

    @GetMapping("/boards")
    public List<Board> findAll(){
        return boardService.getList();
    }

    @GetMapping("boards/{boardId}")
    public Board findById(@PathVariable String boardId) {
        return boardService.getBoardById(boardId);
    }

}