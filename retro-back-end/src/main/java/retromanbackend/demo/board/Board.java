package retromanbackend.demo.board;

import lombok.Builder;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import retromanbackend.demo.user.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "boards")
public class Board {
    @Id
    private String id;
    private String title;
    private String description;
    public ArrayList<User> participant = new ArrayList<>();

    @DBRef
    private User user;
}