package retromanbackend.demo.board;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import retromanbackend.demo.exception.BoardNotFoundException;
import retromanbackend.demo.section.*;
import retromanbackend.demo.user.*;

import org.springframework.stereotype.Service;

@Service
public class BoardService {

    private BoardRepository boardRepository;
    private SectionService sectionService;
    private UserService userService;

    public BoardService(BoardRepository boardRepository, UserService userService, SectionService sectionService) {
        this.boardRepository = boardRepository;
        this.sectionService = sectionService;
        this.userService = userService;
    }

    public Board saveById(String userId, Board board) {
        User user = userService.findById(userId);
        board.setUser(user);
        ArrayList<User> participant = new ArrayList<>();
        participant.add(user);
        board.setParticipant(participant);
        Board createdBoard = boardRepository.save(board);
        sectionService.createDefaultSection(createdBoard);
        return createdBoard;
    }

    public List<Board> getList() {
        return this.boardRepository.findAll();
    }

    public List<Board> getListByUserId(String userId) {
        User user = userService.findById(userId);
        return boardRepository.findAllByUser(user);
    }

    public Board getBoardById(String id) {
        Board board = boardRepository.findById(id).orElseThrow(BoardNotFoundException::new);
        return board;
    } 

    public Board joinBoard(String boardId, String userId) {
        User user = userService.findById(userId);
        Board board = boardRepository.findById(boardId).get();
        ArrayList<User> participant = board.getParticipant();
        if(!participant.contains(user)) {
            participant.add(user);
            board.setParticipant(participant);
            boardRepository.save(board);
        }
        return board;
    }
}