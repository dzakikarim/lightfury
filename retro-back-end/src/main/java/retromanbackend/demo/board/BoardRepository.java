package retromanbackend.demo.board;

import java.util.List;

import retromanbackend.demo.user.*;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface BoardRepository extends MongoRepository<Board, String> {

	List<Board> findAllByUser(User user);
}