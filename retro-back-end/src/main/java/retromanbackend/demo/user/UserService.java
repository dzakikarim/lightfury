package retromanbackend.demo.user;

import retromanbackend.demo.exception.*;

import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository users) {
        this.userRepository = users;
    }

    public User findById(String id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElseThrow(UserNotFoundException::new);
    }

    public User updateById(String id, User user) {
        user.setId(id);
        userRepository.save(user);

        return user;
    }

    public User deleteById(String id) {
        User user = findById(id);
        userRepository.deleteById(id);
        return user;

    }
}