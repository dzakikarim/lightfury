package retromanbackend.demo.user;

import java.util.List;
import retromanbackend.demo.exception.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@EnableSwagger2
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @PostMapping("/users")
    public ResponseEntity<User> create(@RequestBody User user) {
        User createdUser = userRepository.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @GetMapping("/login")
    public ResponseEntity<User> LogIn(@RequestParam String username, @RequestParam String password) {
        User logInUser = userRepository.findByUsername(username);
        if (password.equals(logInUser.getPassword()) && logInUser != null) {
            return ResponseEntity.status(HttpStatus.OK).body(logInUser);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping("/users")
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public User findOneUserById(@PathVariable String id) throws UserNotFoundException {
        return userService.findById(id);
    }

    @PutMapping("/users/{id}")
    public User updateById(@RequestBody User user, @PathVariable String id) {
        user.setId(id);
        return userService.updateById(id, user);
    }

    @DeleteMapping("/users/{id}")
    public User deletebyId(@PathVariable String id) {
        return userService.deleteById(id);
    }

}