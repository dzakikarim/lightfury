package retromanbackend.demo.user;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * UserRepository
 */
public interface UserRepository extends MongoRepository<User, String>{

	User findByUsername(String username);
    
}