package retromanbackend.demo.card;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import retromanbackend.demo.section.Section;
import retromanbackend.demo.user.User;

@Data
@AllArgsConstructor
@Builder
@Document(collection = "cards")
public class Card {

    @Id
    private String id;
    private String argument;
    public ArrayList<User> likes = new ArrayList<>();

    @DBRef
    private Section section;

    @DBRef
    private User user;
}