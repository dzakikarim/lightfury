package retromanbackend.demo.card;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import retromanbackend.demo.section.Section;
import retromanbackend.demo.section.SectionService;
import retromanbackend.demo.user.User;
import retromanbackend.demo.user.UserRepository;
import retromanbackend.demo.user.UserService;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@CrossOrigin
@RestController
@EnableSwagger2
public class CardController {
    @Autowired
    private CardService cardService;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private UserService userService;

    @PostMapping("/sections/{sectionId}/cards/{userId}")
    public ResponseEntity<Card> create(@PathVariable String sectionId, @PathVariable String userId,
            @RequestBody Card card) {
        Section section = sectionService.findById(sectionId);
        User user = userService.findById(userId);
        card.setSection(section);
        card.setUser(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(cardService.saveBySectionId(card));
    }

    @PostMapping("/cards/{cardId}/likes/{userId}")
    public Card addLikes(@PathVariable String cardId, @PathVariable String userId) {
        return cardService.addLikes(cardId, userId);
    }

    @GetMapping("/sections/{sectionId}/cards")
    public List<Card> findAllBySectionId(@PathVariable String sectionId) {
        return cardService.getCardBySectionId(sectionId);
    }

    @DeleteMapping("/cards/{cardId}/users/{userId}")
    public Card deleteByUserId(@PathVariable String cardId, @PathVariable String userId) {
        User user = userService.findById(userId);
        Card card = cardService.findById(cardId);

        if (card.getUser().getId().equals(user.getId())) {
            return cardService.deleteById(cardId);
        }
        return card;
    }
}