package retromanbackend.demo.card;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import retromanbackend.demo.section.Section;

public interface CardRepository extends MongoRepository<Card, String>{

	List<Card> findAllBySection(Section section);

}