package retromanbackend.demo.card;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import retromanbackend.demo.exception.CardNotFoundException;
import retromanbackend.demo.section.Section;
import retromanbackend.demo.section.SectionService;
import retromanbackend.demo.user.User;
import retromanbackend.demo.user.UserService;

@Service
public class CardService {

    private CardRepository cardRepository;
    private SectionService sectionService;
    private UserService userService;

    public CardService(CardRepository cardRepository, SectionService sectionService, UserService userService) {
        this.cardRepository = cardRepository;
        this.sectionService = sectionService;
        this.userService = userService;
    }

    public Card findById(String id) {
        Optional<Card> user = cardRepository.findById(id);
        return user.orElseThrow(CardNotFoundException::new);
    }

    public Card saveBySectionId(@RequestBody Card card) {
        ArrayList<User> likes = new ArrayList<>();
        card.setLikes(likes);
        cardRepository.save(card);
        return card;
    }

    public List<Card> getCardBySectionId(String sectionId) {
        Section section = sectionService.findById(sectionId);
        return cardRepository.findAllBySection(section);
    }

    public Card deleteById(String cardId) {
        Card deletedCard = findById(cardId);
        cardRepository.deleteById(cardId);
        return deletedCard;
    }

    public Card addLikes(String cardId, String userId) {
        User user = userService.findById(userId);
        Card card = findById(cardId);
        ArrayList<User> likes = card.getLikes();
        if (!likes.contains(user)) {
            likes.add(user);
        }
        else{
            likes.remove(user);
        }
        card.setLikes(likes);
        cardRepository.save(card);
        return card;
    }
}