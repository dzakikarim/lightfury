package retromanbackend.demo.section;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface SectionRepository extends MongoRepository<Section, String> {

	List<Section> findAllByBoard(Object created); 
}