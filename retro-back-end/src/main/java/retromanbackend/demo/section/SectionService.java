package retromanbackend.demo.section;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import retromanbackend.demo.board.*;
import retromanbackend.demo.exception.SectionNotFoundException;

import org.springframework.stereotype.Service;

@Service
public class SectionService {
    private SectionRepository sectionRepository;
    private BoardRepository boardRepository;

    public SectionService(SectionRepository sectionRepository, BoardRepository boardRepository) {
        this.sectionRepository = sectionRepository;
        this.boardRepository = boardRepository;
    }

    public void createDefaultSection(Board board) {
        Section wentWell = Section.builder().title("Went Well").board(board).build();
        Section needToImprove = Section.builder().title("Need to Improve").board(board).build();
        Section actionItems = Section.builder().title("Action Items").board(board).build();
        sectionRepository.saveAll(Arrays.asList(wentWell, needToImprove, actionItems));
    }

    public List<Section> getSectionByBoardId(String boardId) {
        Optional<Board> board = boardRepository.findById(boardId);
        return sectionRepository.findAllByBoard(board);
    }

    public Section findById(String id) {
        Section section = sectionRepository.findById(id).orElseThrow(SectionNotFoundException::new);
        return section;
    }

}