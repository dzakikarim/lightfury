package retromanbackend.demo.section;

import retromanbackend.demo.board.*;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "sections")
public class Section {
    @Id
    private String id;
    
    private String title;  

    @DBRef
    private Board board;  
}
