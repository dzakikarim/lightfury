package retromanbackend.demo.section;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@CrossOrigin
@RestController
@EnableSwagger2
public class SectionContoller {

    @Autowired
    SectionService sectionService;

    @GetMapping("boards/{boardId}/sections")
    public List<Section> getSectionByBoardId(@PathVariable String boardId) {
        return sectionService.getSectionByBoardId(boardId);
    }

}